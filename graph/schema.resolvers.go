package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"log"
	"my-test/graph/generated"
	"my-test/graph/model"
	"my-test/graph/service"
)

func (r *mutationResolver) AddToCard(ctx context.Context, input *model.NewAddToCard) (*model.Cart, error) {
	data, err := service.AddCartSave(input)
	log.Println(err)
	if err != nil {
		return data, err
	}
	return data, nil
}

func (r *mutationResolver) CheckOut(ctx context.Context, input *model.NewCheckOut) (*model.ListCheckOut, error) {
	data, err := service.CheckOutSave(input)
	log.Println(err)
	if err != nil {
		return data, err
	}
	return data, nil
}

func (r *queryResolver) Products(ctx context.Context) ([]*model.Product, error) {
	var results []*model.Product

	result, err := service.FindAllProduct()
	if err != nil {
		return nil, err
	}
	for _, Product := range result {
		results = append(results, &model.Product{ID: Product.ID, Sku: Product.Sku, Name: Product.Name, Price: Product.Price, Qty: Product.Qty})
	}
	return results, nil
}

func (r *queryResolver) Discount(ctx context.Context) ([]*model.Discount, error) {
	var results []*model.Discount

	result, err := service.FindAllDiscount()
	if err != nil {
		return nil, err
	}
	for _, Discount := range result {
		results = append(results, &model.Discount{ID: Discount.ID, NoSku: Discount.NoSku, Description: Discount.Description, Condition: Discount.Condition, Required: Discount.Required, Type: Discount.Type, Value: Discount.Value})
	}
	return results, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
