package graph

//go:generate go run github.com/99designs/gqlgen
import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Resolver struct {
	DB *gorm.DB
}
