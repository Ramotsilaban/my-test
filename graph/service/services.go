package service

import (
	"fmt"
	"log"
	"my-test/database"
	"my-test/graph/model"
	"strconv"
)

func AddCartSave(input *model.NewAddToCard) (*model.Cart, error) {
	find := model.Cart{}
	product := model.Product{}
	db := database.DbManager()
	err := db.Table("product").First(&product, "sku = ?", input.NoSku)
	if err.Error != nil {
		return nil, err.Error
	}

	if product.Qty >= input.Qty {

		q := db.Table("cart").First(&find, "no_transaction = ? and no_costumer = ? and no_sku = ?", input.NoTransaction, input.NoCostumer, input.NoSku)
		if q.RowsAffected == 0 {
			add := &model.Cart{
				NoTransaction: input.NoTransaction,
				NoCostumer:    input.NoCostumer,
				NoSku:         input.NoSku,
				Qty:           input.Qty,
				Status:        false,
			}
			err := db.Table("cart").Create(add)
			if err.Error != nil {
				return nil, err.Error
			}
			return add, nil
		} else {
			return nil, fmt.Errorf("%s", "Data already exists!")
		}
	} else {
		return nil, fmt.Errorf("%s", "Stock product quantity is not enough!!")
	}
}

func CheckOutSave(input *model.NewCheckOut) (*model.ListCheckOut, error) {
	cart := []model.Cart{}
	db := database.DbManager()

	find := db.Table("cart").Find(&cart, "no_transaction = ? and no_costumer = ?", input.NoTransaction, input.NoCostumer)
	if find.Error != nil {
		return nil, find.Error
	}

	findCheck := model.CheckOut{}
	get := db.Table("check_out").First(&findCheck, "no_transaction = ?", input.NoTransaction)

	typeA := "FreeProduct"
	typeB := "multiple"
	typeC := "persent"

	var Total float64

	if find.RowsAffected != 0 && get.RowsAffected == 0 {
		for _, n := range cart {
			m, _ := FindDiscountBySku(n.NoSku)
			if m.Type == typeA {
				data, _ := FindProductBySKu(n.NoSku)
				if data.Qty >= n.Qty {
					if n.Qty >= m.Required {
						temp, _ := FindProductByID(int(m.Value))
						if temp.Qty >= n.Qty {
							add := &model.Cart{
								NoTransaction: input.NoTransaction,
								NoCostumer:    input.NoCostumer,
								NoSku:         temp.Sku,
								Qty:           n.Qty,
								Status:        true,
							}
							err := db.Table("cart").Create(add)
							if err.Error != nil {
								return nil, err.Error
							}
							Total += data.Price * float64(n.Qty)
							UpdateQytProduct(data.ID, n.Qty)
							UpdateStatusCart(input.NoTransaction, n.NoSku, true)

						} else {
							Total += data.Price * float64(n.Qty)
							UpdateQytProduct(data.ID, n.Qty)
							UpdateStatusCart(input.NoTransaction, n.NoSku, true)

						}
					} else {
						Total += data.Price * float64(n.Qty)
						UpdateQytProduct(data.ID, n.Qty)
						UpdateStatusCart(input.NoTransaction, n.NoSku, true)

					}
				}
			} else if m.Type == typeB {
				data, _ := FindProductBySKu(n.NoSku)
				if data.Qty >= n.Qty {
					if n.Qty >= m.Required {

						totaldiscount := float64((n.Qty-(n.Qty%m.Required))/m.Required) * m.Value

						sum := data.Price*float64(n.Qty) - totaldiscount

						Total += sum
						UpdateQytProduct(data.ID, n.Qty)
						UpdateStatusCart(input.NoTransaction, n.NoSku, true)

					} else {
						Total += data.Price * float64(n.Qty)

						UpdateQytProduct(data.ID, n.Qty)
						UpdateStatusCart(input.NoTransaction, n.NoSku, true)
					}
				}
			} else if m.Type == typeC {
				data, _ := FindProductBySKu(n.NoSku)
				if data.Qty >= n.Qty {
					if n.Qty >= m.Required {
						sum := (data.Price * float64(n.Qty))
						Total += sum - (sum * float64(m.Value/100))
						UpdateQytProduct(data.ID, n.Qty)
						UpdateStatusCart(input.NoTransaction, n.NoSku, true)
					} else {
						Total += (data.Price * float64(n.Qty))
						UpdateQytProduct(data.ID, n.Qty)
						UpdateStatusCart(input.NoTransaction, n.NoSku, true)
					}
				}
			}

		}

		ParseTotal := fmt.Sprintf("%.2f", Total)
		Total, _ := strconv.ParseFloat(ParseTotal, 64)

		if get.RowsAffected == 0 {
			add := model.CheckOut{
				NoTransaction: input.NoTransaction,
				Total:         Total,
			}
			db.Table("check_out").Create(&add)
		}

		findCheck := model.CheckOut{}
		db.Table("check_out").First(&findCheck, "no_transaction = ?", input.NoTransaction)

		item := model.ListCheckOut{}
		item.NoTransaction = findCheck.NoTransaction
		item.Item = []*model.Cart{}
		item.Total = findCheck.Total

		db.Table("cart").Find(&item.Item, "no_transaction = ?", input.NoTransaction)
		return &item, nil
	} else {
		return nil, fmt.Errorf("%s", "Product in Cart is not Found! OR Product already Check Out")
	}
}

func FindAllProduct() ([]model.Product, error) {
	var a model.Product
	var b []model.Product
	conn := database.DbManager()
	result, err := conn.DB().Prepare("select id, sku, name, price, qty from product")

	if err != nil {
		log.Fatal(err)
	}
	rows, err := result.Query()
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		err = rows.Scan(&a.ID, &a.Sku, &a.Name, &a.Price, &a.Qty)
		if err != nil {
			panic(err.Error())
		}
		b = append(b, a)
	}
	return b, nil
}

func FindAllDiscount() ([]model.Discount, error) {
	var a model.Discount
	var b []model.Discount
	conn := database.DbManager()
	result, err := conn.DB().Prepare("select id, no_sku, description, condition, required, type, value from discount")

	if err != nil {
		log.Fatal(err)
	}
	rows, err := result.Query()
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		err = rows.Scan(&a.ID, &a.NoSku, &a.Description, &a.Condition, &a.Required, &a.Type, &a.Value)
		if err != nil {
			panic(err.Error())
		}
		b = append(b, a)
	}
	return b, nil
}

func UpdateQytProduct(id int, qyt int) error {
	conn := database.DbManager()
	find := model.Product{}
	q := conn.Table("product").First(&find, id)
	if q.Error != nil {
		return q.Error
	}

	total := find.Qty - qyt
	if find.Qty >= qyt {
		q := conn.Table("product").Where("id = ?", id).Update("qty", total)
		if q.Error != nil {
			return q.Error
		}
		return nil
	} else {
		return fmt.Errorf("%s", "Stock product quantity is not enough!")
	}
}

func FindProductByID(id int) (model.Product, error) {
	conn := database.DbManager()
	find := model.Product{}
	q := conn.Table("product").Find(&find, "id = ?", id)
	if q.Error != nil {
		return find, q.Error
	}
	return find, nil
}

func FindProductBySKu(sku string) (model.Product, error) {
	conn := database.DbManager()
	find := model.Product{}
	q := conn.Table("product").Find(&find, "sku= ?", sku)
	if q.Error != nil {
		return find, q.Error
	}
	return find, nil
}

func UpdateStatusCart(no_transaction int, no_sku string, status bool) error {
	conn := database.DbManager()
	q := conn.Table("cart").Where("no_transaction = ? and no_sku = ?", no_transaction, no_sku).Update("status", status)
	if q.Error != nil {
		return q.Error
	}
	return nil
}

func FindDiscountBySku(sku string) (model.Discount, error) {
	conn := database.DbManager()
	find := model.Discount{}
	q := conn.Table("discount").First(&find, "no_sku= ?", sku)
	if q.Error != nil {
		return find, q.Error
	}
	return find, nil
}
