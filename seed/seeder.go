package main

import (
	db "my-test/database"
	"my-test/graph/model"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

//data product seeder
var product = []model.Product{
	{
		ID:    1,
		Sku:   "1200P90",
		Name:  "Google Home",
		Price: 49.99,
		Qty:   10,
	},
	{
		ID:    2,
		Sku:   "43N23P",
		Name:  "MacBook Pro",
		Price: 5339.99,
		Qty:   5,
	},
	{
		ID:    3,
		Sku:   "A304SD",
		Name:  "Alexa Speaker",
		Price: 109.50,
		Qty:   10,
	},
	{
		ID:    4,
		Sku:   "234234",
		Name:  "Raspberry Pi B",
		Price: 30.00,
		Qty:   2,
	},
}

//data Discount seeder
var discount = []model.Discount{
	{
		ID:          1,
		NoSku:       "43N23P",
		Description: "Each sale of a MacBook Pro comes with a free Raspberry Pi B",
		Condition:   "equal",
		Required:    1,
		Type:        "FreeProduct",
		Value:       4,
	},
	{
		ID:          2,
		NoSku:       "1200P90",
		Description: "Buy 3 Google Homes for the price of 2",
		Condition:   "equal",
		Required:    3,
		Type:        "multiple",
		Value:       49.99,
	},
	{
		ID:          3,
		NoSku:       "A304SD",
		Description: "Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers",
		Condition:   "more than",
		Required:    3,
		Type:        "persent",
		Value:       10,
	},
}

// main for execute seeder
func main() {
	db.Init()
	conn := db.DbManager()
	defer conn.Close()

	for _, k := range product {
		conn.Table("product").Create(&k)
	}

	for _, k := range discount {
		conn.Table("discount").Create(&k)
	}
}
