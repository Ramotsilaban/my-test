package database

import (
	"fmt"
	"my-test/config"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB
var err error

func Init() string {
	configuration := config.GetConfig()
	connect_string := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		configuration.DB_HOST, configuration.DB_PORT, configuration.DB_USER, configuration.DB_NAME, configuration.DB_PASSWORD, configuration.DB_SSL_MODE)
	db, err = gorm.Open("postgres", connect_string)

	if err != nil {
		panic("DB Connection Error")
	}
	return connect_string
}

func DbManager() *gorm.DB {
	db.LogMode(true)
	return db
}
