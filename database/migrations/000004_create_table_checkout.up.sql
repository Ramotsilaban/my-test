CREATE TABLE check_out (
  id serial NOT NULL PRIMARY KEY,
  no_transaction varchar NOT NULL,
  total float
);