CREATE TABLE cart (
  id serial PRIMARY KEY,
  no_transaction varchar NOT NULL,
  no_costumer varchar NOT NULL,
  no_sku varchar,
  qty int,
  status boolean
);