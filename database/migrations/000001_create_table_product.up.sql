CREATE TABLE product (
  id serial NOT NULL PRIMARY KEY,
  sku varchar NOT NULL,
  name varchar(100) NOT NULL,
  price float NOT NULL,
  qty int
);
