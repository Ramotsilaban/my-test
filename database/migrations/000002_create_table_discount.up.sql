CREATE TABLE discount (
  id serial NOT NULL PRIMARY KEY,
  no_sku varchar NOT NULL,
  description varchar,
  condition varchar NOT NULL,
  required int NOT NULL,
  type varchar NOT NULL,
  value float
);