migrate-db-up:
	@go run cmd/cli/cli.go db:migrate --up
migrate-db-down:
	@go run cmd/cli/cli.go db:migrate --down
db-seeder:
	@go run seed/seeder.go 