module my-test

go 1.16

require (
	github.com/99designs/gqlgen v0.17.2
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/jinzhu/gorm v1.9.16
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	github.com/urfave/cli/v2 v2.3.0
	github.com/vektah/gqlparser/v2 v2.4.0
)
