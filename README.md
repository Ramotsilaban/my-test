## How to run local

requeiment go = Go version go1.16.6

1. Make sure Go is installed and you have configured the GOPATH.

2. Also make sure you have installed PostgreSQL locally on your machine, create a username+password and a fresh new DB. Keep this information in handy (username, password, and DB name).

3. Change the contents of config.json in the config folder to match your database

4. Run database migration using this command:

   migrate-db-up:
   @go run cmd/cli/cli.go db:migrate --up

5. Insert Data, run seeder.go in folder seed, with this command :

   db-seeder:
   @go run seed/seeder.go

6. Run app with this command:

   go mod init

   go mod vendor

   go mod tidy

   go run server.go

7. Access the api with graphQL. http://localhost:8080.
